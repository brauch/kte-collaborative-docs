\documentclass[a4]{scrartcl}

\usepackage[english]{babel}
\usepackage{metre}

\input{header}

\newcommand{\thetitle}{kte-collaborative: Collaborative text editing in KTextEditor and kde-telepathy}
\newcommand{\theauthor}{Sven Brauch}
\newcommand{\adv}{{\color{main} \asteriscus}}
\newcommand{\arr}{\(\rightarrow~\)}


\begin{document}
\addtokomafont{sectioning}{\rmfamily}

% \addtokomafont{disposition}{\color{main}}

\title{\thetitle}
\author{\theauthor}
\date{\today}
\maketitle

\begin{abstract}
kte-collaborative is a project which provides support for collaborative text editing in KTextEditor and enables you to edit documents collaboratively with your Instant Messenger contacts without the need for a centralized server or manual connection setup. It is based on the KTextEditor interfaces for editing text, on the libinfinity library for the protocol and the collaborative editing part, and on kde-telepathy for widgets and functionality related to sharing documents with instant messenger contacts.
\end{abstract}

\section{Purpose of this document}
This document is aims to be an advanced user's guide to the program. The main part of the document is meant for pure users of the software, although some sections might be too technical to be of interest for some users.

Sections which are probably mostly relevant for technically interested users are marked with \adv.

\graph[0.5]{images/splitview.png}{Example screenshot of kte-collaborative running in kate, displaying three documents from the gobby.0x539.org server.}{}

\clearpage

\tableofcontents

\clearpage

\section{Purpose of the software}
\subsection{Things we want to do}
kte-collaborative was designed with the following aims in mind:
\begin{itemize}
\item Enable the average ``non-technical'' KDE user who already uses kde-telepathy as his instant messenger to collaboratively edit documents with his Jabber/XMPP contacts, without requiring any network-related setup. A typical example use case would be planning a vacation, and writing down what needs to be done in a collaborative document.
\item Allow the same thing also for ``technical'' use cases, i.\,e. provide a text editor which is suitable for editing program- or markup code. Typical example use cases include explaining the syntax of a programming language to someone, or writing a university homework in \LaTeX{} together.
\item Provide a tool to KDE developers for code review or related purposes. A typical example for this would be a new contributor working on a cpp file in KDevelop for a while, but then running into a problem, and needing help from a more experienced developer. Instead of uploading his changed copy of the document to a pastebin service, and other people downloading that pastebin, changing it and uploading it again, it should be possible for the new contributor to easily share his document for collaborative editing right from KDevelop, and other people should with the same ease be able to look at the document from KDevelop.
\item Finally, the project aims to provide a full-featured KDE-based client for the infinote protocol (which is thus compatible with e.\,g. gobby). This implies that we aim to support all \emph{essential} features the protocol, but not that we aim to provide a tool suitable for every obscure use case someone can come up with.
\end{itemize}

\subsection{Things we do not care about}
The project at the current state does explicitly \emph{not} care about the following things, although that might change in the future:
\begin{itemize}
\item Performance with more than 25 active users.
\item Performance with documents larger than 20.000 lines or 500.000 characters.
\item Being bandwidth-efficient.
\item Having a small set of dependencies.
\end{itemize}

\section{Installation}
Since installation procedure is so different between different systems, it will not be described in detail here; just short hints are given.
\subsection{Using a package manager}
Usually, you should be able to install this software by searching your package manager for a package named \texttt{kte-collaborative} or similar. Install the package, and log out and back in again. Then, launch e.\,g. Kate to start using the program.

\subsection{By building from source code \adv}
You can get the source code from the KDE servers using \texttt{git clone}:
\begin{itemize}
\item for libqinfinity (dependency): \texttt{git://anongit.kde.org/libqinfinity.git}
\item for kte-collaborative: \texttt{git://anongit.kde.org/kte-collaborative.git}
\end{itemize}
Notable dependencies include the \texttt{libinfinity} and, if this is packaged separately in your distribution, \texttt{infinoted} packages.

In this case, too, a variety of services needs to be restarted and that requires a variety of commands to do properly; thus, it is probably a good idea to restart your computer or at least log out of your session after installing the plugin to ensure everything works as intended.

\subsection{Notes for packaging \adv}
kde-telepathy-meta should include or at least recommend this package. Additionally to the dependencies which are obvious, the infinote server application, \texttt{infinoted}, currently is a hard compile- and runtime dependency (CMake should verify it is present at compile time), so the package must depend on the package containing this application.

\section{The user interface}
kte-collaborative does not contain a dedicated editor application by itself. Instead, it provides a plugin for the KTextEditor interface, which especially means it will work in any application using KatePart. Prominent examples of such applications include Kate itself, but also KWrite, Kile, and KDevelop. In each of these applications, you should have a ``Collaborative'' menu like in \fig{collabmenu} when the software is installed correctly (if not, see \ref{menu_missing}).
\graph[0.7]{images/menu.png}{The user interface the plugin adds to a host application when it is loaded.}{collabmenu}

\subsection{The ``Share document'' button}
\lemph{This button is only enabled when you are not editing a collaborative document.}

\graph[0.8]{images/sharedialog.png}{The ``Share document'' dialog.}{sharedialog}
This dialog, displayed in \fig{sharedialog}, is used to publish a document and make it available to remote users. For details on what the buttons do, refer to Section \ref{sharedocument}.

\subsection{The ``Open file manager'' button}
\lemph{This button is only enabled when you are editing a collaborative document.}

\label{openfilemanager}
Opens dolphin at the place where the current collaborative document is saved, and thus enables you to add more documents there.

\subsection{The ``Disconnect'' button}
\lemph{This button is only enabled when you are editing a collaborative document.}

Disconnects from the current session, and saves the current buffer contents somewhere in \texttt{/tmp}. Changes you do will no longer be synchronized to the server, and you will not receive any changes from other users.

\subsection{The ``Save a local copy'' button}
\lemph{This button is only enabled when you are editing a collaborative document.}

Saves the document to a local file, but stays connected to the collaborative session. Asks for a location the first time you trigger it, future calls will overwrite that document.

\subsection{The ``Change user name'' button}
\lemph{This button is only enabled when you are editing a collaborative document.}

Change your user name, i.\,e. the name you appear with for remote users to a different one. For technical reasons, this will disconnect you from the session and re-join it with the new user name. The way colors are picked also implies that you will have a different color.

\subsection{The ``Open collaborative document'' button}
\label{opendialog}
\graph[0.8]{images/opendialog.png}{The ``Open document'' dialog.}{opendialog}
This dialog, pictured in \fig{opendialog}, is used to open a collaborative document, either from an existing Telepathy-initiated connection, or from a classic server. The ``Advanced'' button offers some advanced options, such as using a different port. For more information, refer to Section \ref{opendocument}.

An example public server is \texttt{gobby.0x539.de}.

\subsection{The ``Configure'' button}
\graph[0.8]{images/settingsdialog.png}{The Settings dialog.}{settingsdialog}
This button opens the configuration module, as seen in \fig{settingsdialog}. For more information on the configuration options it provides, see Section \ref{configuration}.

\subsection{The status bar}
\graph[0.7]{images/statusbar.png}{The status bar below the document.}{statusbar}
The status bar below the document, as seen in \fig{statusbar}, currently indicates
\begin{itemize}
\item the connection status of the session (connected, loading, disconnected);
\item and the users which are part of the session.
\end{itemize}
If the status bar is not present, then the document you are editing is not a collaborative document.

The users list shows a little colored box indicating the color the user will appear with in the document, and the user's name. Your own name and color is marked with the suffix ``(you)'' behind your name. Offline users are marked with the ``(offline)'' suffix and by the right lower half of their color box being grayed out.

If more users are online than fit into your status bar, the users list will collapse and only show users' color boxes. Moving the mouse over one of the boxes will show the user name in a tooltip. If even the collapsed form would not fit into the status bar, only the amount of users is displayed instead.

Clicking the ``Users'' button gives two options for customizing who is displayed in the users list:
\begin{itemize}
\item Checking \emph{Show offline users} will also show all users which have been in the session at some point in the past, but currently are not. This setting is disabled by default.
\item Checking \emph{Show users without contributions} will show all users which currently have text belonging to them in the document, or had text belonging to them since you joined the session. Disabling this checkbox is useful to reduce the list to the participating users in case e.\,g. three people are writing a protocol and 20 are watching. This setting is enabled by default.
\end{itemize}
Thus, by default, the toolbar will simply display everyone who is online; but you can for example make it display everyone who wrote a part of the document in the past instead by setting ``Show offline users'' to true and ``Show users without contributions'' to false.

\section{Different ways to share a document}
\label{sharedocument}
There are two fundamtentally different ways to share a document with someone:
\begin{itemize}
\item By adding it to an existing server.
\item By using Telepathy to build a connection to a Jabber contact or chatroom.
\end{itemize}
In either case, a connection will be built and you can share an arbitrary number of documents and folders over this same connection. For this purpose, you can for example use the ``Open file manager'' button (see \ref{openfilemanager}) and just copy documents into the window which opens.

\subsection{Technical details of Telepathy-based connections \adv}
\label{connections_technical}
Even in the peer-to-peer connections built by Telepathy, there currently always is a server running. The server is run by the person initiating the request. The server is called \lemph{infinoted} and is started by kte-collaborative in the background in this case. Telepathy then establishes the connection and binds the port the server is listening on on your computer to some port on the remote computer, with black magic to actually transfer data between the two. This black magic can work in three ways:
\begin{itemize}
\item If you are in the same local network as the target or have a publically reachable IP address, the connection is established directly.
\item Otherwise, an attempt is made to use a proxy server for the connection.
\item If both fails, an attempt is made to build an \lemph{in-band bytestream} connection, which transfers data base64-encoded inside XMPP stanzas, and thus basically uses the jabber server as a proxy.
\end{itemize}
In any of the above cases, all parties will end up having a local port on their computer (e.\,g. 127.0.0.1:41531) with all data flowing through that port being transferred to the others by Telepathy. Thus, all documents from Telepathy-built connection always end up having 127.0.0.1 as their host address.

For the same reason, the peer-to-peer connections have exactly the same properties as the classical client-server connections, especially regarding having multiple documents and folders in one connection.

\section{Sharing a document using Jabber/XMPP}
\subsection{Sharing a document with one contact}
\label{share_with_contact}
To share a document with a contact, first open the document you want to share. Then open the ``Share document'' dialog, and click ``Share document with contact''. You will be prompted to select a contact from your contact list. The capability to collaboratively edit a document is advertised on XMPP; thus only contacts which actually have the program installed and configured correctly will be selectable.

The other person will receive a notification about the request, and can choose to deny or accept it. Your editor should immediately load the collaborative document (see \fig{loading}). The other person should join shortly after.
\graph[1.0]{images/loading.png}{The ``splash screen'' of the plugin while loading a collaborative document}{loading}

\subsection{Sharing a document with a chatroom}
\label{share_with_chatroom}
This works the same as sharing a document with a contact; just use the ``Share document with chat room'' button. Contacts joining the chatroom later on will be offered to join the collaborative session, too.

Note that due to the effects described in Section \ref{connections_technical}, when the user who initiated the session disconnects, all users will be disconnected.

\subsection{Opening an existing document from a Telepathy-initiated connection}
When you accidentially or purposefully close a document on a connection initiated as described in Sections \ref{share_with_contact} and \ref{share_with_chatroom}, the connection is not closed; instead, it stays alive in the background and you can re-open the document using the ``Open document'' dialog (\fig{opendialog}): the widget in the lower part of the dialog should list the connection to the contact, and you can click it to get an ordinary ``Open file'' dialog listing all files in the connection.

\subsection{Adding a document to an existing connection}
Similar to what was described in the previous section, you can add a document to a connection initiated as described in Sections \ref{share_with_contact} and \ref{share_with_chatroom} by using the ``Share document'' dialog (\fig{sharedialog}). Other users in the connection will be notified of this and are given opportunity to join the session immediately, or at any later time using the technique described in the previous section.

\subsection{Technical details of establishing a connection using Telepathy \adv}
Telepathy uses a technology called \emph{Tubes} to establish bytestream connections between two arbitrary instant messenger contacts. This is currently only implemented for Jabber/XMPP, though. Section \ref{connections_technical} lists the possibilities of how such a connection can work internally.

For the user of the API, in this case kte-collaborative, Telepathy offers a DBus interface to request such \emph{Channels} (a Tube is a special Channel). The details of Telepathy Channel dispatching are quite complicated, but a short outline of how it works in kte-collaborative shall be given here:
\begin{itemize}
\item Some application, for example Kate or also ktp-text-ui (kde-telepathy's chat window application), requests a new Tube channel with type ``infinote''. In the parameters of the channel requests, it specifies which documents should be opened initially and whether an editor needs to be launched on the initiating side. The latter is set to true for requesting channels from ktp-text-ui, and to false for requesting channels from e.\,g. Kate.
\item First, this request is handled by an application called \texttt{infinoteservertubehandler} \emph{on the same computer}. If not running, the application is launched via DBus activation. It takes care of starting the infinote server (see also Section \ref{starting_a_server}), then tells Telepathy to proceed sending the request to the other person.
\item The channel request is received by the other person; an application called \texttt{infinote\-client\-tubehandler} (without the dash) is started, and takes care of launching the editor on the client's computer as soon as Telepathy indicates that the connection was built successfully.
\item Closing an editor window has no effect whatsoever on the connection. A connection is closed when your Jabber account disconnects, or when one of the tube handler applications is terminated for some reason.
\end{itemize}
For debugging purposes, you can start the client- and server tube handler applications (they are located in your \texttt{libexec}/ directory) by hand.

\subsection{Technical details of the notifications \adv}
There's a separate daemon called \texttt{infinotenotifier} which is launched and watches all connections for documents being added (the infinote protocol provides push-noti\-fi\-cations for this purpose, so just watching a connection consumes no bandwidth). When it detects that a document was added to an existing connection, it displays a notification to the user. You can disable those -- as always -- in KDE's systemsettings program, under Notifications.

\section{Using a classical server to share a document}
A \emph{classical server} is defined here as a server (as in physical machine) standing somewhere running an infinote server, whilst having a public IP address and probably a domain name. An example for such a classical server is \texttt{gobby.0x539.de}.

The obvious disadvantage of such a server is that you need a centralized place where this server is located; thus you either need to run it yourself, or all your documents will most likely be public and editable by everyone. The former most likely requires you to rent a server somewhere, which isn't free, and also requires some setup effort. Still, there are cases where centralized servers are useful; especially when you want a set of documents to be available over a longer period of time.

\subsection{Opening an existing document from a classical server}
\label{opendocument}
\graph[0.8]{images/opendialog_manual.png}{The ``Open collaborative document'' dialog with manually filled out connection parameters}{opendialog_manual}
The ``Open collaborative document'' dialog, as described in Section \ref{opendialog}, can be used to conveniently connect to a classical server (see \fig{opendialog_manual}). In the normal case, just enter the server host name in the ``Remote host address'' field and click Ok. You will be presented a list with documents on the server; click one to join it.

\subsection{Adding a document to a classical server}
\label{shareclassical}
\graph[0.8]{images/sharedialog_manual.png}{The ``Share document'' dialog after clicking the ``Upload document to an existing server'' button}{sharedialog_manual}
To upload a document to an existing server, you can use the ``Share document'' dialog in much the same way (\fig{sharedialog_manual}): Click the ``Upload document to an existing server'' button, enter the server parameters, and select a file name. Your editor will automatically join the newly created document.

\subsection{Re-using parameters for classical servers}
In case you frequently use the same server, you do not have to enter the server address each time you want to load or create a document there. The dialog described in sections \ref{opendocument} and \ref{shareclassical} are basically just a user interface to create an URL of the form \begin{center}\texttt{inf://user:password@servername:port/}\end{center} or, in a simpler case \begin{center}\texttt{inf://servername/}\end{center} which then opens a file selection dialog for that base URL. Since those are just perfectly normal URLs, you can create a bookmark for them as usual, and use that bookmark to refer to the server later. For this purpose, just use the "Add bookmark" function as usual, by right-clicking the bookmark bar in the file dialog (see \fig{save_params})
\graph[0.7]{images/save_params.png}{To save a set of parameters, just open a dialog which shows the server's files as described in sections \ref{opendocument} and \ref{shareclassical}, and add a bookmark.}{save_params}

You can now just use \emph{the normal Open and Save As actions} you'd use to open or save local files to open or create collaborative documents, by just clicking e.\,g. File \arr Open and then your bookmark. You do not need to use the (more verbose) Collaborative \arr \dots\, dialogs any more.

Note that this does currently not work for Telepathy-initiated connections. Those will have a different URL format every time you connect.

\subsection{Technical details on \texttt{inf://} URLs \adv}
As you might have guessed, there's a KIO slave which handles the \texttt{inf://} protocol. Knowing this, you can do more fun things. For example, you can open collaborative documents from the command line:
\begin{center}\texttt{kate inf://myusername@gobby.0x539.de/main.cpp}\end{center}
It gets even more powerful when you additionally use kioclient:
\begin{center}\texttt{kioclient cp myfile.cpp inf://gobby.0x539.de/myfile.cpp}\\
\texttt{kioclient ls inf://gobby.0x539.de/test1/}\end{center}
The KTextEditor plugin will treat any document with an URL which starts with \texttt{inf://} as a collaborative document and will attempt to synchronize it. It will use the user name and password provided in the URL as your user name and password in the session. It does not care how you open that URL.

\subsection{Starting an infinote server \adv}
\label{starting_a_server}
The server executable is called \emph{infinoted} or sometimes \emph{infinoted-0.5} or \emph{infinoted-0.6}. You can just run it to get an infinote server running on localhost, like this for example:
\begin{center}\texttt{mkdir /tmp/document\_root}\\
\texttt{infinoted --security-policy=no-tls -r /tmp/document\_root}\\\end{center}
You can then use it to collaboratively edit documents with yourself:
\begin{center}\texttt{kioclient cp test.cpp inf://localhost/test.cpp}\\
\texttt{kate inf://localhost/test.cpp}\end{center}
If you have a public IP or can control your router well enough, you can edit documents with people in this way.

\section{Configuration options}
\label{configuration}
kte-collaborative offers a few configuration options to adjust the program to your needs. \fig{settingsdialog} shows the settings dialog.

\subsection{Changing the text colors}
Changing the ``Saturation'' slider will make the colors used to highlight which user wrote what more or less intense (dragging it to the right means more intense colors). You need to re-join the session for this setting to take effect.
If you don't like the colored text background, you can also turn it off completely here.

A useful feature related to this can be found in the context menu of the view: right-click anywhere in the document and click ``Clear user highlights'' to clear all the background highlighting. That way, when new changes are made you can easily spot them.

\subsection{Changing the display of the popup widgets}
By default, the plugin will draw small popup widgets when a user types text to indicate who is typing (see \fig{popup_widget}). You can disable them by unticking the box.
\graph[0.6]{images/popup_widgets.png}{Popup widgets telling which user is writing stuff}{popup_widget}

\subsection{Changing the display of the tooltips}
There's an option to display tooltips which tell what user wrote what text when you move the mouse over that text (see \fig{tooltips}). Since it can easily be annoying while editing, you can turn it off by unticking the box. For this, too, you need to close and re-open the document (better restart the editor, since this setting is bound to the view which is not guaranteed to be recreated on re-opening the document).
\graph[0.4]{images/tooltip.png}{Tooltips tell which user wrote the text under the mouse}{tooltips}

\subsection{Changing the default application for Telepathy-initated connections}
You can also select the application you want to be started when initiating a connection from Telepathy here (this matters when a contact shares a document with you via Jabber or when you share a document via the button in the KTp chat window). There's various editors listed, including Gobby; you can also just open the folder in Dolphin and then proceed from there.

The first time you edit a collaborative document using a Telepathy-initiated connection, a dialog will ask you about your preferred editor application. The setting you pick there is the same as the one in the settings dialog.

If you have an application you'd like to be added to the list, let me know.

\section{Useful tips}
\subsection{Document sidebar}
One thing you probably want if you have more than one document on a server (especially when you're working with a classic server) is a sidebar which displays all available documents. In Kate, to get this, first open a document from the server you want to work on. Then, go to Settings \arr Configure Kate \arr Plugins and enable ``File system browser''. That should give a tab in the left sidebar which opens a file system browser. Click the ``sync'' icon (shown in \fig{sidebar}) to make the sidebar display the items from the server. You can also use the ``Favorites'' feature to make a bookmark for this server, to easily open it next time.
\graph[0.5]{images/sidebar.png}{The ``File system browser'' sidebar, which is available in some applications such as Kate an KDevelop, can easily be used as a browser for collaborative documents.}{sidebar}

\subsection{Sharing multiple files}
This isn't exactly new if you read the rest of the document, but not very obvious, so it might be worth stating: If you have one document on a server, you can always click Collaborative \arr Open file manager, and then just copy some files or even whole folders there. For example, you could copy the whole kte-collaborative repository tree there, to have it available for editing with others.

Be aware though that copying files to such a server is computionally expensive and will take some time. You'd better not try copying images or build files there. It'll probably work, but it will be slow and useless.

\paragraph{Sharing multiple files without opening a document \adv} Knowing how the \texttt{inf://} URLs work, you can of course just open e.\,g. \texttt{inf://gobby.0x539.de/} in dolphin, create a folder there, and copy some files over.

\section{Troubleshooting}
Whatever happens, if something goes wrong after you changed anything related to your Telepathy or kte-collaborative installation (especially when you just installed the program), please restart your computer and try if it works after that. Services should automatically reload when their configuration changes, but many just don't do it correctly.

\subsection{The ``Collaborative'' Menu isn't there}
\label{menu_missing}
The collaborative editing plugin is not loaded. Look for the ``Editor \arr Extensions'' configuration module in your application (in Kate you can find it in ``Settings \arr Configure Kate'') and make sure the ``Collaborative text editing'' plugin is present and enabled, as shown in \fig{kteplugin}.
\graph[0.7]{images/plugin.png}{KTextEditor plugin settings module as it appears in Kate.}{kteplugin}

\subsection{Creating a Telepathy-based connection fails or breaks after a while \adv}
Shortly put, it's a bug, please let us know about it. It would be very very useful to have the following information:
\begin{itemize}
\item The text in the ``Gabble'' tab of the ``ktp-debugger'' program (type the latter in a console to get it) from \emph{both} sides of the connection attempt
\item The Jabber server(s) you were using.
\end{itemize}

Going a bit more into detail, this is a difficult topic, and there's a few known issues right now, especially these:
\begin{itemize}
\item Jabber.org seems to be quite broken for in-band bytestreams and sometimes loses packets. We can't do much about this.
\item Randomly stream-initation fails with a ``initiator does not have tube capabilities'' error from gabble; reason is unknown, we're investigating. For the user, this shows as a "Connection refused" error box when sharing a document.
\item Few servers provide a working proxy so often the not-too-great in-band bytestream solution is used as a fallback; we're working on getting a proxy list online.
\end{itemize}
Either way, when you encounter one of these errors, please let us know and include the information mentioned above -- it's very helpful to get more data points about these errors.

\end{document}




















